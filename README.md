# README

### What is this repository for?

This is a dummy cordova plugin used to reproduce the non detection of certain hooks on windows.

In the plugin.xml file, these hooks are declared:

 * `before_plugin_add`
 * `after_plugin_add`
 * `before_plugin_rm`
 * `after_plugin_rm`

### Problem

`before_plugin_add` and `after_plugin_rm` hooks are not executed, while being [documented](https://cordova.apache.org/docs/en/latest/guide/appdev/hooks/index.html).

(On a production project, `after_plugin_add` is not executed too on windows, while it is executed on a linux or mac machine.)

### How do I get set up?

 * Clone this project
 * Create a cordova project
 * Finally add this plugin

tldr
`cordova create testapp && cd testapp && cordova plugin add ..\cordova-plugin-hook-bug --nofetch -d && cordova plugin rm cordova-plugin-hook-bug`


### Output on plugin add

C:\Users\Guillaume\test>cordova create testapp && cd testapp && cordova plugin add ..\..\cordova-plugin-hook-bug --nofetch -d

Creating a new cordova project.

**No scripts found for hook "before_plugin_add".**

Calling plugman.fetch on plugin "..\..\cordova-plugin-hook-bug"

Copying plugin "..\..\cordova-plugin-hook-bug" => "C:\Users\Guillaume\test\testapp\plugins\cordova-plugin-hook-bug"

Adding cordova-plugin-hook-bug to package.json

Saved plugin info for "cordova-plugin-hook-bug" to config.xml

Executing script found in plugin cordova-plugin-hook-bug for hook "after_plugin_add": plugins\cordova-plugin-hook-bug\testhook.js

**Hook executed: after_plugin_add**

### Output on plugin rm

C:\Users\Guillaume\test\testapp>cordova plugin rm cordova-plugin-hook-bug

**Hook executed: before_plugin_rm**

Removing "cordova-plugin-hook-bug"

Removing plugin cordova-plugin-hook-bug from config.xml file...

Removing cordova-plugin-hook-bug from package.json